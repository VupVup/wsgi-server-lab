import asyncore
import asynchat
import socket
import multiprocessing
import logging
import mimetypes
import os
import argparse
from time import strftime, gmtime


def url_normalize(path):
    if path.startswith("."):
        path = "/" + path
    while "../" in path:
        p1 = path.find("/..")
        p2 = path.rfind("/", 0, p1)
        if p2 != -1:
            path = path[:p2] + path[p1+3:]
        else:
            path = path.replace("/..", "", 1)
    path = path.replace("/./", "/")
    path = path.replace("/.", "")
    return path


class FileProducer(object):

    def __init__(self, file, chunk_size=4096):
        self.file = file
        self.chunk_size = chunk_size

    def more(self):
        if self.file:
            data = self.file.read(self.chunk_size)
            if data:
                return data
            self.file.close()
            self.file = None
        return ""


class AsyncServer(asyncore.dispatcher):

    def __init__(self, host="127.0.0.1", port=9000):
        super().__init__()
        self.serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
        self.serv_sock.bind((host, port))
        self.serv_sock.listen(10)
        pass

    def handle_accepted(self, sock, addr):
        AsyncHTTPRequestHandler(sock)
        pass

    def serve_forever(self):
        while True:
            client_sock, client_addr = self.serv_sock.accept()
            self.handle_accepted(client_sock, client_addr)
            asyncore.loop()
        pass


class AsyncHTTPRequestHandler(asynchat.async_chat):

    def __init__(self, sock):
        super().__init__(sock)
        self.set_terminator(b"\r\n\r\n")
        self.reading_headers = False
        self.method = ''
        self.inc_data = []
        self.http_param = {}
        self.body = ''
        self.path = f'{os.getcwd()}/test_www'

    def collect_incoming_data(self, data):
        self.data = data
        self._collect_incoming_data(data)

    def found_terminator(self):
        self.parse_request()

    def parse_request(self):
        if not self.reading_headers:
            self.parse_headers()
            if self.method not in ['GET', 'HEAD', 'POST']:
                self.send_error(405, 'Bad request')
            if self.method == 'POST':
                if int(self.http_param['Content-Length']) > 0:
                    self.set_terminator(int(self.http_param['Content-Length']))
                    self.body = self.ac_in_buffer.decode('utf-8')
                else:
                    self.handle_request()
            else:
                self.handle_request()
        else:
            self.handle_request()

    def parse_headers(self):
        self.inc_data.extend(self.data.decode('utf-8').split('\n'))
        self.http_protocol = self.inc_data[0].split(' ')[2]
        self.method = self.inc_data[0].split(' ')[0]
        self.reading_headers = True
        for part in self.inc_data[1:]:
            self.http_param.update({part.split(': ')[0]:part.split(': ')[1]})

    def handle_request(self):
        method_name = 'do_' + self.method
        if not hasattr(self, method_name):
            self.send_error(405)
            self.handle_close()
            return
        handler = getattr(self, method_name)
        handler()

    def date_time_string(self):
        return strftime('%a, %d %b %Y %H:%M:%S %Z', gmtime())

    def send_response(self, code, message=None):
        http_response = f"{self.http_protocol} {code} {message}\r\n"
        http_response_bytes = str.encode(http_response)
        self.push(http_response_bytes)
        server_headers = [
            ('Date', f'{self.date_time_string()}'),
            ('Server', 'MyServer???????'),
        ]
        for header in server_headers:
            self.send_header(*header)

    def send_header(self, keyword, value):
        header = str.encode(f'{keyword}: {value}\r\n')
        self.push(header)

    def end_headers(self):
        self.send(b"\r\n")

    def send_error(self, code, message=None):
        try:
            short_msg, long_msg = self.responses[code]
        except KeyError:
            short_msg, long_msg = '???', '???'
        if message is None:
            message = short_msg

        self.send_response(code, message)
        self.send_header("Content-Type", "text/plain")
        self.send_header("Connection", "close")
        self.end_headers()

    responses = {
        200: ('OK', 'Request fulfilled, document follows'),
        400: ('Bad Request',
              'Bad request syntax or unsupported method'),
        403: ('Forbidden',
              'Request forbidden -- authorization will not help'),
        404: ('Not Found', 'Nothing matches the given URI'),
        405: ('Method Not Allowed',
              'Specified method is invalid for this resource.'),
    }

    def get_filename(self, path):
        if path.endswith('/') or not path.count('.'):
            return None
        else:
            name = path.split('/')[-1]
            if name.count('?'):
                name = name[:name.find('?')]
                self.path = self.path[:self.path.find('?')]
            if name.count('%20'):
                name = name.replace('%20', ' ')
                self.path = self.path.replace('%20', ' ')
            return name

    def do_GET(self):
        self.path += self.inc_data[0].split(' ')[1]
        self.path = url_normalize(self.path)
        self.name = self.get_filename(self.path)
        if not self.path.startswith('/Users/vladislav/Desktop/Proga/python/wsgi-server/test_www/'):
            self.send_error(403)
            self.close()
        elif self.name is None:
            if not os.path.exists(self.path):
                self.send_error(404)
                self.close()
            elif "index.html" in os.listdir(self.path):  # if index.html does exist
                with open(f'{self.path}index.html', "rb") as f:
                    statinfo = os.stat(f'{self.path}index.html')
                    self.send_response("200", "OK")
                    self.send_header("Content-Type", "text/plain")
                    self.send_header("Content-Length", statinfo.st_size)
                    self.end_headers()
                    self.push_with_producer(FileProducer(f))
                    self.close()
            else:
                self.send_error(403)
                self.close()
        else:
            try:
                with open(f'{self.path}', "rb") as f:
                    statinfo = os.stat(f'{self.path}')
                    self.send_response("200", "OK")
                    self.send_header("Content-Length", statinfo.st_size)
                    self.send_header("Content-Type", mimetypes.guess_type(self.name)[0])
                    self.end_headers()
                    for i in range((statinfo.st_size // 4096)+1):
                        self.push_with_producer(FileProducer(f))
            except:
                self.send_error(404)
            finally:
                self.close()
        self.close()

    def do_HEAD(self):
        self.path += self.inc_data[0].split(' ')[1]
        self.path = url_normalize(self.path)
        self.name = self.get_filename(self.path)
        if not self.path.startswith('/Users/vladislav/Desktop/Proga/python/wsgi-server/test_www/'):
            self.send_error(403)
            self.close()
        elif self.name is None:
            if not os.path.exists(self.path):
                self.send_error(404)
                self.close()
            elif "index.html" in os.listdir(self.path):  # if index.html does exist
                with open(f'{self.path}index.html', "rb") as f:
                    statinfo = os.stat(f'{self.path}index.html')
                    self.send_response("200", "OK")
                    self.send_header("Content-Type", "text/plain")
                    self.send_header("Content-Length", statinfo.st_size)
                    self.end_headers()
                    self.close()
            else:
                self.send_error(403)
                self.close()
        else:
            try:
                with open(f'{self.path}', "rb") as f:
                    statinfo = os.stat(f'{self.path}')
                    self.send_response("200", "OK")
                    self.send_header("Content-Length", statinfo.st_size)
                    self.send_header("Content-Type", mimetypes.guess_type(self.name)[0])
                    self.end_headers()
            except:
                self.send_error(404)
            finally:
                self.close()
        self.close()


def parse_args():
    parser = argparse.ArgumentParser("Simple asynchronous web-server")
    parser.add_argument("--host", dest="host", default="127.0.0.1")
    parser.add_argument("--port", dest="port", type=int, default=9000)
    parser.add_argument("--log", dest="loglevel", default="info")
    parser.add_argument("--logfile", dest="logfile", default=None)
    parser.add_argument("-w", dest="nworkers", type=int, default=1)
    parser.add_argument("-r", dest="document_root", default=".")
    return parser.parse_args()

def run():
    server = AsyncServer(host=args.host, port=args.port)
    server.serve_forever()

if __name__ == "__main__":
    args = parse_args()

    logging.basicConfig(
        filename=args.logfile,
        level=getattr(logging, args.loglevel.upper()),
        format="%(name)s: %(process)d %(message)s")
    log = logging.getLogger(__name__)

    DOCUMENT_ROOT = args.document_root
    for _ in range(args.nworkers):
        p = multiprocessing.Process(target=run)
        p.start()